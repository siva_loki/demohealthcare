# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

class UserProfile:
    """
      This is our application state. Just a regular serializable Python class.
    """

    def __init__(self, name: str = None, department: str = None,date : str= None, mobile: int = 0, email: str = None):
        self.name = name
        self.department = department
        self.date = date
        self.mobile = mobile
        self.email = email
