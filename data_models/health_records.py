
class HealthRecords:
    """
      This is our application state. Just a regular serializable Python class.
    """

    def __init__(self, name: str = None, id: int = None, weight: int = 0,):
        self.name = name
        self.id = id
        self.weight = weight
