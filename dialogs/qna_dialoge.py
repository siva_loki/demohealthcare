from botbuilder.core import MessageFactory
from botbuilder.dialogs import (
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult,
    ComponentDialog)
from botbuilder.dialogs.prompts import (TextPrompt,PromptOptions )
from botbuilder.ai.qna import QnAMaker, QnAMakerEndpoint
from config import DefaultConfig as conf
from .cancel_and_help_dialog import CancelAndHelpDialog


class QnaDialog(CancelAndHelpDialog):
    def __init__(self, dialog_id: str = None):
        super(QnaDialog, self).__init__(dialog_id or QnaDialog.__name__)

        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.qna_maker = QnAMaker(
            QnAMakerEndpoint(
                knowledge_base_id=conf.QNA_KNOWLEDGEBASE_ID,
                endpoint_key=conf.QNA_ENDPOINT_KEY,
                host=conf.QNA_ENDPOINT_HOST, )
                )
        self.add_dialog(WaterfallDialog(WaterfallDialog.__name__,[self.init_step],))
        self.initial_dialog_id = WaterfallDialog.__name__

    async def welcome_qna_step(self,step_context : WaterfallStepContext) -> DialogTurnResult:
        # Here we can set welcome message
        # WaterfallStep always finishes with the end of the Waterfall or with another
        # dialog, here it is the end.
        return await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions())  # currently we are processing empty "

    async def init_step(self,step_context: WaterfallStepContext) -> DialogTurnResult:
        # WaterfallStep always finishes with the end of the Waterfall or with another
        # dialog, here it is the end.
        luis_required= step_context.options
        # if luis_required:
        #     await step_context.prompt(TextPrompt.__name__,PromptOptions())
        response = await self.qna_maker.get_answers(step_context.context)
        if response and len(response) > 0:
            await step_context.context.send_activity(MessageFactory.text(response[0].answer))

            return await step_context.end_dialog()

        else:
                await step_context.context.send_activity("No QnA Maker answers were found.")
                return await step_context.end_dialog()




