import base64
import os
import urllib

from botbuilder.core import MessageFactory
from botbuilder.dialogs import (WaterfallDialog, WaterfallStepContext, DialogTurnResult, ComponentDialog, PromptOptions,
                                AttachmentPrompt)
from botbuilder.dialogs.prompts import TextPrompt,NumberPrompt
from botbuilder.schema import Attachment
from requests import request
import json


class CreateUser(ComponentDialog):
    def __init__(self,dialog_id: str = None):
        super(CreateUser,self).__init__( dialog_id or CreateUser.__name__)
        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(AttachmentPrompt(AttachmentPrompt.__name__))
        self.add_dialog(NumberPrompt(NumberPrompt.__name__))
        self.add_dialog(WaterfallDialog(WaterfallDialog.__name__,[self.init_step,self.act_step,self.profile_step,self.final_step]))
        self.initial_dialog_id=WaterfallDialog.__name__

    async def init_step(self,step_context: WaterfallStepContext) -> DialogTurnResult:

        await step_context.context.send_activity(MessageFactory.text("Welcome ,Please provide following details"))
        return await step_context.prompt(
            TextPrompt.__name__,PromptOptions(prompt=MessageFactory.text("Please enter your name")))

    async def act_step(self,step_context: WaterfallStepContext) -> DialogTurnResult:

            self.name = step_context.result

            return await step_context.prompt(TextPrompt.__name__,
                                             PromptOptions(prompt=MessageFactory.text("Please enter your job")))

    async def profile_step(self,step_context: WaterfallStepContext) -> DialogTurnResult:
        self.job =  step_context.result
        prompt_options = PromptOptions(
            prompt=MessageFactory.text(
                "Please attach a profile picture (or type any message to skip)."
            ),
            retry_prompt=MessageFactory.text(
                "The attachment must be a jpeg/png image file."
            ),
        )
        return await step_context.prompt(AttachmentPrompt.__name__, prompt_options)

    async def final_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:

            profile =(None if not step_context.result else step_context.result[0])
            attachment_info = await self.save_attachment(profile)

            userprofile = await self._get_inline_attachment(attachment_info.get('local_path'),attachment_info.get('filename'))

            url = "https://reqres.in/api/users"
            headers = {
                'Content-Type': 'application/json'
                        }
            data=dict()
            data["name"],data["job"]=self.name,self.job
            payload = json.dumps(data)

            response = request("POST", url, headers=headers, data=payload)
            if response:
                result=response.json()
                msg= (f'\n Name : { result["name"] }\n'
                      f'\n Job: { result["job"] }\n'
                      f'\n Id : { result["id"] }\n'
                      f'\n Date : { result["createdAt"] } \n')
                await step_context.context.send_activity(MessageFactory.text("Congrats, Your Profile Was Created !"))
            else:
                 msg =(" Sorry, Please Check your Connection")

            await step_context.context.send_activity(MessageFactory.text(msg))
            if userprofile:
                await step_context.context.send_activity(
                    MessageFactory.attachment(
                        userprofile, "This is your profile picture."
                    )
                )
            else:
                await step_context.context.send_activity(
                    "A profile picture was saved but could not be displayed here."
                )

            return await step_context.end_dialog()

    async def save_attachment(self,attachment: Attachment) -> dict:
        try:
            response = urllib.request.urlopen(attachment.content_url)
            headers = response.info()
            if headers["content-type"] == "application/json":
                data = bytes(json.load(response)["data"])
            else:
                data = response.read()

            local_filename = os.path.join(os.getcwd(), "resource\image.png")
            with open(local_filename, "wb") as out_file:
                out_file.write(data)

            return {"filename": attachment.name, "local_path": local_filename}

        except Exception as exception:
            print(exception)
            return {}

    async def _get_inline_attachment(self,path,name) -> Attachment:
            """
            Creates an inline attachment sent from the bot to the user using a base64 string.
            Using a base64 string to send an attachment will not work on all channels.
            Additionally, some channels will only allow certain file types to be sent this way.
            For example a .png file may work but a .pdf file may not on some channels.
            Please consult the channel documentation for specifics.
            :return: Attachment
            """
            file_path =path
            # file_path = os.path.join(os.getcwd(), "resources/architecture-resize.png")
            with open(file_path, "rb") as in_file:
                base64_image = base64.b64encode(in_file.read()).decode()

            return Attachment(
                name=name,
                content_type="image/png",
                content_url=f"data:image/png;base64,{base64_image}",
            )
