from botbuilder.dialogs import (
    ComponentDialog,
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult, ListStyle, )

from botbuilder.dialogs.prompts import (
    TextPrompt,
    NumberPrompt,
    ChoicePrompt,
    ConfirmPrompt,
    AttachmentPrompt,
    PromptOptions,
    PromptValidatorContext,
)
from botbuilder.dialogs.choices import Choice
from botbuilder.core import MessageFactory, UserState
from botbuilder.schema import InputHints

from .qna_dialoge import QnaDialog
from .appointment_dialog import AppointmentDialog
from .health_records_dialog import HealthDialog
from luis_recognizer import LuisAppRecognizer
from helpers.luis_helper import LuisHelper, Intent,top_intent
from .luis_dialog import LuisDialog
from .create_user_dialog import CreateUser
from .get_user_details_dialog import GetUser
from .media_dialog import MediaDialog
from data_models import LuisRequired

class MainDialog(ComponentDialog):
    def __init__(self,booking : AppointmentDialog, qna: QnaDialog , health :HealthDialog , luis : LuisAppRecognizer,luis_dialog : LuisDialog,
                 create_user : CreateUser ,get_user: GetUser ,media : MediaDialog):
        super(MainDialog, self).__init__(MainDialog.__name__)
        self._luis_recognizer = luis
        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog((media))
        self.media_id=media.id
        self.add_dialog(get_user)
        self.get_user_id=get_user.id
        self.add_dialog(create_user)
        self.create_user_id= create_user.id
        self.qna_id=qna.id
        self.booking_id=booking.id
        self.add_dialog(qna)
        self.add_dialog(ChoicePrompt(ChoicePrompt.__name__,MainDialog.Choice_validator))
        self.add_dialog(booking)
        self.add_dialog(health)
        self.health_id=health.id
        self.add_dialog(luis_dialog)
        self.luis_id=luis_dialog.id
        self.add_dialog(
            WaterfallDialog(
                "WFDialog", [self.intro_step, self.act_step,self.final_step])
        )

        self.initial_dialog_id = "WFDialog"

    async def intro_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # We are geeting input to redirect the respective dialog flow

        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(prompt=MessageFactory.text("How can I assist you today ?"),
                          choices=[Choice("Appointment"), Choice("FAQ's"),Choice("Health Records"),Choice("Create User")
                                   ,Choice("Get User"),Choice("Media")],
                          style=ListStyle.hero_card,
                          ),
        )

    async def act_step(self,step_context: WaterfallStepContext) -> DialogTurnResult:

            if not self._luis_recognizer.is_configured:
                if (step_context.context.activity.text).lower() == "appointment":
                    return await step_context.begin_dialog(self.booking_id)
                if (step_context.context.activity.text).lower() == "health records":
                    return await step_context.begin_dialog(self.health_id)
                if (step_context.context.activity.text).lower() in ["faq","faq's"]:
                    await step_context.context.send_activity("Please Ask your quires")
                    return await step_context.begin_dialog(self.luis_id)
                else:
                    await step_context.context.send_activity(
                        MessageFactory.text(
                            "NOTE: LUIS is not configured. To enable all capabilities, add 'LuisAppId', 'LuisAPIKey' and "
                            "'LuisAPIHostName' to the appsettings.json file.",
                            # input_hint=InputHints.ignoring_input,
                        )
                    )

                    await step_context.context.send_activity("Your are redirected to FAQ'S")
                    return await step_context.begin_dialog(self.luis_id)

            if (step_context.context.activity.text).lower() == "create user":
                return await step_context.begin_dialog(self.create_user_id)
            if (step_context.context.activity.text).lower() == "get user":
                return await step_context.begin_dialog(self.get_user_id)
            if (step_context.context.activity.text).lower() == "media":
                return await step_context.begin_dialog(self.media_id)

            intent, luis_result = await LuisHelper.execute_luis_query(
                    self._luis_recognizer, step_context.context)
            if intent == "appointment" or (step_context.context.activity.text).lower() == "appointment" :
                return await step_context.begin_dialog(self.booking_id)
            if intent == "records" or (step_context.context.activity.text).lower() == "health records":
                return await step_context.begin_dialog(self.health_id)
            if intent == "faq" or (step_context.context.activity.text).lower() in ["faq","faq's"] :
                await step_context.context.send_activity("Please Ask your quires")
                return await step_context.begin_dialog(self.luis_id)
            else:
                # await step_context.context.send_activity("qna Coming")
                await step_context.context.send_activity("Hi, i am Lius,Please Ask your quires")
                return await step_context.begin_dialog(self.luis_id)

    async def final_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:

            return await step_context.replace_dialog("WFDialog")

    @staticmethod
    async def Choice_validator(prompt_context: PromptValidatorContext) -> bool:
        # This condition is our validation rule. You can also change the value at this point.
        if prompt_context.options.prompt.text is not None:
            return True
