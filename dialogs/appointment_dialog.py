import re
import os
import json
from botbuilder.dialogs import (
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult, ListStyle,ComponentDialog)

from botbuilder.ai.qna import QnAMaker, QnAMakerEndpoint
from config import DefaultConfig

from botbuilder.dialogs.prompts import (
    TextPrompt,
    NumberPrompt,
    ChoicePrompt,
    ConfirmPrompt,
    PromptOptions,
    PromptValidatorContext,)

from botbuilder.dialogs.choices import Choice
from botbuilder.core import MessageFactory, UserState
from botbuilder.schema import Attachment
from .cancel_and_help_dialog import CancelAndHelpDialog
from data_models import UserProfile


class AppointmentDialog(CancelAndHelpDialog):
    def __init__(self, user_state: UserState,dialog_id: str = None):
        super(AppointmentDialog, self).__init__(dialog_id or AppointmentDialog.__name__)
        self.user_profile_accessor = user_state.create_property("UserProfile")

        self.add_dialog(TextPrompt(TextPrompt.__name__, AppointmentDialog.email_prompt_validator))
        self.add_dialog(
            NumberPrompt(NumberPrompt.__name__, AppointmentDialog.mobile_prompt_validator))
        self.add_dialog(ChoicePrompt(ChoicePrompt.__name__))
        self.add_dialog(ConfirmPrompt(ConfirmPrompt.__name__))
        self.add_dialog(
            WaterfallDialog(
                WaterfallDialog.__name__,
                [
                    self.name_step,
                    self.department_step,
                    self.Appoinment_step,
                    self.date_step,
                    self.mobile_step,
                    self.email_step,
                    self.confirm_step,
                    self.summary_step,
                ],
            )
        )
        self.initial_dialog_id = WaterfallDialog.__name__

    async def name_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # WaterfallStep always finishes with the end of the Waterfall or
        # with another dialog; here it is a Prompt Dialog.
        return await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions(prompt=MessageFactory.text("May I know your name please?")),)

    async def department_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # Storing previous result into name variable
        step_context.values["name"] = step_context.result
        # We can send messages to the user at any point in the WaterfallStep.
        await step_context.context.send_activity(
            MessageFactory.text(f"Thanks {step_context.result}"))
        # WaterfallStep always finishes with the end of the Waterfall or
        # with another dialog; here it is a Prompt Dialog.
        return await step_context.prompt(
            ChoicePrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("What treatment are you looking for?"),
                choices=[Choice("Skin"), Choice("Hair"), Choice("Dental"), Choice("Bones"), Choice("Cardiovascular")],
                style=ListStyle.hero_card
            ),
        )

    async def Appoinment_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # Storing previous result into department variable
        step_context.values["department"] = step_context.result.value
        await step_context.context.send_activity(
            MessageFactory.text("You can visit us at any time from 10:00 AM to 10:00PM. We are open on all days"))
        # WaterfallStep always finishes with the end of the Waterfall or
        # with another dialog; here it is a Prompt Dialog.
        return await step_context.prompt(
            ConfirmPrompt.__name__,
            PromptOptions(prompt=MessageFactory.text("Would you like to continue with the appointment?")),)

    async def date_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        if step_context.result:
            prompt_options = PromptOptions(
                prompt=MessageFactory.attachment(
                    self.create_adaptive_card_attachment()))
            return await step_context.prompt(TextPrompt.__name__, prompt_options)

        else:
            await step_context.context.send_activity(
                MessageFactory.text("No worries! Do get back to us in case of any assistance"))
            return await step_context.end_dialog()

    async def mobile_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        if step_context.result:
            date = json.loads(step_context.result)  # here we are converting string into dict
            # Storing previous result into date variable
            step_context.values["date"] = date["date"]
            # WaterfallStep always finishes with the end of the Waterfall or with another dialog,
            # here it is a Prompt Dialog.
            return await step_context.prompt(
                NumberPrompt.__name__,
                PromptOptions(
                    prompt=MessageFactory.text("Please provide your contact number"),
                    retry_prompt=MessageFactory.text(
                        "Please provide valid number without containing country code(+91)"
                    ),
                ),
            )

    async def email_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # Storing previous result into mobile variable
        step_context.values["mobile"] = step_context.result
        # User said "yes" so we will be prompting for the age.
        # WaterfallStep always finishes with the end of the Waterfall or with another dialog,
        # here it is a Prompt Dialog.
        return await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions(
                prompt=MessageFactory.text("Please provide your mail id"),
                retry_prompt=MessageFactory.text(
                    "Please enter valid mail"
                ),
            ),
        )

    async def confirm_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # Storing previous result into email variable
        step_context.values["email"] = step_context.result
        # Get the current profile object from user state.  Changes to it
        # will saved during Bot.on_turn.
        user_profile = await self.user_profile_accessor.get(
            step_context.context, UserProfile)

        user_profile.name = step_context.values["name"]
        user_profile.department = step_context.values["department"]
        user_profile.date = step_context.values["date"]
        user_profile.mobile = step_context.values["mobile"]
        user_profile.email = step_context.values["email"]

        message = (f'I have following details for you \n'
                   f'\n Name : {user_profile.name} \n'
                   f'\n Treatment : {user_profile.department} \n'
                   f'\n Booking Date : {user_profile.date} \n'
                   f'\n Mobile : {user_profile.mobile} \n'
                   f'\n Email : {user_profile.email} ')

        await step_context.context.send_activity(
            MessageFactory.text(message))
        # WaterfallStep always finishes with the end of the Waterfall or
        # with another dialog; here it is a Prompt Dialog.
        return await step_context.prompt(
            ConfirmPrompt.__name__,
            PromptOptions(prompt=MessageFactory.text("Please Confirm")),
        )

    async def summary_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # if "yes" hitted summary will pop up else it begins from intial step
        if step_context.result:
            await step_context.context.send_activity(
                MessageFactory.text("Thank you! We will get in touch with you soon!"))
            # return await step_context.begin_dialog(WaterfallDialog.__name__)
            return await step_context.end_dialog()
        else:
            # WaterfallStep always finishes with the end of the Waterfall or with another
            # dialog, here it is the end.
            return await step_context.begin_dialog(WaterfallDialog.__name__)

    @staticmethod
    async def mobile_prompt_validator(prompt_context: PromptValidatorContext) -> bool:
        # This condition is our validation rule. You can also change the value at this point.
        number = prompt_context.recognized.value
        return (
                prompt_context.recognized.succeeded
                and len(str(number)) == 10)

    @staticmethod
    async def email_prompt_validator(prompt_context: PromptValidatorContext) -> bool:
        # This condition is our validation rule. You can also change the value at this point.
        if prompt_context.options.prompt.text is not None and (
                (str(prompt_context.options.prompt.text)) == "Please provide your mail id"):
            regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
            match = re.fullmatch(regex, (str(prompt_context.recognized.value)))
            if match:
                return True
            else:
                return False

        else:
            return True

    def create_adaptive_card_attachment(self):
        relative_path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(relative_path, "../cards/date.json")
        with open(path) as in_file:
            card = json.load(in_file)

        return Attachment(
            content_type="application/vnd.microsoft.card.adaptive", content=card)
