from botbuilder.dialogs import (
    ComponentDialog,
    WaterfallDialog,
    WaterfallStepContext,
    PromptOptions ,
    DialogTurnResult)

from botbuilder.dialogs.prompts import (
    TextPrompt,
    NumberPrompt,
    PromptOptions,)

from botbuilder.dialogs.choices import Choice
from botbuilder.core import MessageFactory, UserState
from botbuilder.schema import Attachment
from .cancel_and_help_dialog import CancelAndHelpDialog
from data_models import HealthRecords

class HealthDialog(CancelAndHelpDialog):
    def __init__ (self, user_state: UserState,dialog_id: str = None):
        super(HealthDialog,self).__init__(dialog_id or HealthDialog.__name__)
        self.user_profile_accessor = user_state.create_property("HealthRecords")
        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog((NumberPrompt(NumberPrompt.__name__)))
        self.add_dialog(WaterfallDialog(WaterfallDialog.__name__,
                                            [self.name_step,
                                             self.id_step,
                                             self.summary_step
                                            ]
                                        )
                        )
        self.initial_dialog_id = WaterfallDialog.__name__

    async def name_step(self, step_context :WaterfallStepContext) -> DialogTurnResult:

        return await (
            step_context.prompt(TextPrompt.__name__,
                                PromptOptions(prompt=MessageFactory.text("Please enter your name")

                                            )
                                )
                    )

    async def id_step(self,step_context: WaterfallStepContext) -> DialogTurnResult:
        step_context.values["name"] =step_context.result
        return await(step_context.prompt(NumberPrompt.__name__,
                                         PromptOptions(prompt=MessageFactory.text("Please enter your ID"))))

    async def summary_step(self,step_context: WaterfallStepContext) -> DialogTurnResult:
        step_context.values["id"] = step_context.result

        records = await self.user_profile_accessor.get(step_context.context,HealthRecords)

        records.name = step_context.values["name"]
        records.id = step_context.values["id"]

        message = (f' Hi {records.name}, \n'
                   f'\n Your record is  \n'
                   f'\n Height : 160 cm \n'
                   f'\n weight : 75 kg \n'
                   f'\n BMI : 18.5 \n'
                   f'\n Blood Pressure : 120/80 mmHg \n')

        await step_context.context.send_activity(MessageFactory.text(message))
        return await step_context.end_dialog()