from botbuilder.dialogs import (
    ComponentDialog,
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult, ListStyle, )

from botbuilder.dialogs.prompts import (
    TextPrompt,
    NumberPrompt,
    ChoicePrompt,
    ConfirmPrompt,
    AttachmentPrompt,
    PromptOptions,
    PromptValidatorContext,
)
from botbuilder.dialogs.choices import Choice
from botbuilder.core import MessageFactory, UserState
from botbuilder.schema import InputHints
from .cancel_and_help_dialog import CancelAndHelpDialog
from .qna_dialoge import QnaDialog
from .appointment_dialog import AppointmentDialog
from .health_records_dialog import HealthDialog
from luis_recognizer import LuisAppRecognizer
from helpers.luis_helper import LuisHelper, Intent,top_intent
from data_models import LuisRequired

class LuisDialog(CancelAndHelpDialog):
    def __init__(self, luis: LuisAppRecognizer, booking: AppointmentDialog,health: HealthDialog,faq: QnaDialog,dialog_id: str = None):
        super(LuisDialog, self).__init__(dialog_id or LuisDialog.__name__)
        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(WaterfallDialog(WaterfallDialog.__name__,[self.init_step,self.act_step,self.end_step]))
        self.add_dialog(booking)
        self.add_dialog(health)
        self.add_dialog(faq)
        self.booking_id=booking.id
        self.Health_id =health.id
        self.faq_id = faq.id
        self.luis =luis
        self.initial_dialog_id = WaterfallDialog.__name__

    async def init_step(self, step_context : WaterfallStepContext) -> DialogTurnResult:

        return await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions())

    async def act_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        # await step_context.begin_dialog(self.faq_id)
        # await  step_context.context.send_activity("your are here")
        if str(step_context.context.activity.text)  in ["main menu", "mainmenu"]:
            return await step_context.end_dialog()
        luis_required = LuisRequired()
        if self.luis.is_configured:

            intent, luis_result = await LuisHelper.execute_luis_query(
                self.luis, step_context.context)
            if intent == "appointment":
               return await step_context.begin_dialog(self.booking_id)
            if intent == "records":
              return  await step_context.begin_dialog(self.Health_id)
            else:
                # luis_required =LuisRequired()
                return await step_context.begin_dialog(self.faq_id)

        else:
            return await step_context.begin_dialog(self.faq_id)


    async def end_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:

        return await step_context.replace_dialog(WaterfallDialog.__name__)