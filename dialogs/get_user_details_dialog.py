from botbuilder.core import MessageFactory
from botbuilder.dialogs import (WaterfallDialog, WaterfallStepContext, DialogTurnResult, ComponentDialog, PromptOptions)
from botbuilder.dialogs.prompts import TextPrompt,NumberPrompt
from requests import request
import json


class GetUser(ComponentDialog):
    def __init__(self,dialog_id: str = None):
        super(GetUser,self).__init__( dialog_id or GetUser.__name__)
        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.add_dialog(NumberPrompt(NumberPrompt.__name__))
        self.add_dialog(WaterfallDialog(WaterfallDialog.__name__, [self.init_step,self.final_step]))
        self.initial_dialog_id = WaterfallDialog.__name__

    async def init_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:

        await step_context.context.send_activity(MessageFactory.text("Welcome ,Please provide following details"))
        return await step_context.prompt(
            TextPrompt.__name__, PromptOptions(prompt=MessageFactory.text("Please enter your ID")))

    async def final_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        id = step_context.result
        url = "https://reqres.in/api/users/"
        headers = {}
        payload = ""
        response = request("GET", url+id, headers=headers, data=payload)
        if response:
            result = response.json()
            msg = (f'\n Your Profile :\n'
                    f'\n First Name : {result["data"]["first_name"]}\n'
                   f'\n Last Name: {result["data"]["last_name"]}\n'
                   f'\n Email : {result["data"]["email"]}\n')

        else:
            msg = (" Sorry, Please Check your Connection")

        await step_context.context.send_activity(MessageFactory.text(msg))
        return await step_context.end_dialog()
