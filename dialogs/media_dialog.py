from botbuilder.core import MessageFactory, CardFactory
from botbuilder.dialogs import (
    WaterfallDialog,
    WaterfallStepContext,
    ComponentDialog,
    DialogTurnResult, PromptOptions)
from botbuilder.dialogs.prompts import ChoicePrompt
from botbuilder.dialogs.choices import Choice
from botbuilder.schema import (
    ActionTypes,
    Attachment,
    AnimationCard,
    AudioCard,
    HeroCard,
    VideoCard,
    MediaUrl,
    CardAction,
    ThumbnailUrl,
    CardImage,
    AttachmentLayoutTypes,
)

class MediaDialog(ComponentDialog):
    def __init__(self,dialog_id: str = None):
        super(MediaDialog,self).__init__( dialog_id or MediaDialog.__name__)

        # Define the main dialog and its related components.
        self.add_dialog(ChoicePrompt("CARD_PROMPT"))
        self.add_dialog(
            WaterfallDialog(
                "MAIN_WATERFALL_DIALOG", [self.choice_card_step, self.show_card_step]
            )
        )

        # The initial child Dialog to run.
        self.initial_dialog_id = "MAIN_WATERFALL_DIALOG"

    async def choice_card_step(
        self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        """
        1. Prompts the user if the user is not in the middle of a dialog.
        2. Re-prompts the user when an invalid input is received.
        """

        # Prompt the user with the configured PromptOptions.
        return await step_context.prompt(
            "CARD_PROMPT",
            PromptOptions(
                prompt=MessageFactory.text(
                    "What card would you like to see? You can click or type the card name"
                ),
                retry_prompt=MessageFactory.text(
                    "That was not a valid choice, please select a card or number from 1 "
                    "to 3."
                ),
                choices=self.get_choices(),
            ),
        )
    async def show_card_step(
        self, step_context: WaterfallStepContext
    ) -> DialogTurnResult:
        """
        Send a Rich Card response to the user based on their choice.
        self method is only called when a valid prompt response is parsed from the user's
        response to the ChoicePrompt.
        """
        reply = MessageFactory.list([])

        found_choice = step_context.result.value

        if found_choice == "Audio Card":
            reply.attachments.append(self.create_audio_card())
        elif found_choice == "Image Card":
            reply.attachments.append(self.create_hero_card())
        elif found_choice == "Video Card":
            reply.attachments.append(self.create_video_card())
        else:
            reply.attachment_layout = AttachmentLayoutTypes.carousel
            reply.attachments.append(self.create_audio_card())
            reply.attachments.append(self.create_hero_card())
            reply.attachments.append(self.create_video_card())

        # Send the card(s) to the user as an attachment to the activity
        await step_context.context.send_activity(reply)

        # Give the user instructions about what to do next
        # await step_context.context.send_activity("Type anything to see another card.")

        return await step_context.end_dialog()

    def create_audio_card(self) -> Attachment:
        card = AudioCard(
            media=[MediaUrl(url="https://www.thecommunityguide.org/sites/default/files/assets/CommGuide-Dr-Jamie-Chriqui.mp3")],
            title="Spotlight: Jamie F. Chriqui, PhD, MHS",
            subtitle="Community Guideline",
            text="Dr. Jamie Chriqui has been a Community Preventive Services Task Force member since 2016. She is a professor of health policy and administration in the School of Public Health at the University"
                 " of Illinois at Chicago and a leading authority on public health policy surveillance and evaluation. Listen to her talk with CDC’s Dr."
                 " John Anderton about her experiences and how decision makers at all levels can use CPSTF findings to improve health and safety in their communities.",
            buttons=[
                CardAction(
                    type=ActionTypes.open_url,
                    title="Read more",
                    value="https://www.thecommunityguide.org/content/audio-clips",
                )
            ],
        )
        return CardFactory.audio_card(card)

    def create_video_card(self) -> Attachment:
        card = VideoCard(
            title="Health technology",
            subtitle="Description",
            text="Medical doctor scientist COVID vaccine researcher wearing mask and suit with smart mobile virus analysis,"
                 " medical laboratory IoT technology AI mobile health care digital futuristic presentation.",
            media=[
                MediaUrl(
                    url="https://media.istockphoto.com/videos/medical-doctor-scientist-covid-vaccine-researcher-wearing-mask-and-video-id1225146260"
                )
            ],
            buttons=[
                CardAction(
                    type=ActionTypes.open_url,
                    title="Learn More",
                    value="https://www.istockphoto.com/search/search-by-video?assetid=1168154992",
                )
            ],
        )
        return CardFactory.video_card(card)

    def create_hero_card(self) -> Attachment:
        card = HeroCard(
            title="",
            images=[
                CardImage(
                    url="https://img.etimg.com/thumb/msid-59550685,width-300,imgsize-158948,,resizemode-4,quality-100/.jpg"
                )
            ],
            # buttons=[
            #     CardAction(
            #         type=ActionTypes.open_url,
            #         title="Get Started",
            #         value="https://docs.microsoft.com/en-us/azure/bot-service/",
            #     )
            # ],
        )
        return CardFactory.hero_card(card)

    def get_choices(self):
        card_options = [
            Choice(value="Audio Card", synonyms=["audio"]),
            Choice(value="Image Card", synonyms=["Image"]),
            Choice(value="Video Card", synonyms=["video"]),
            Choice(value="All Cards", synonyms=["all"]),
        ]

        return card_options