# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.
from botbuilder.core import MessageFactory, TurnContext
from botbuilder.schema import ChannelAccount

from helpers.dialog_helper import DialogHelper
from .dialog_bot import DialogBot


class DialogAndWelcomeBot(DialogBot):
    def __init__(self, conversation_state, user_state, dialog):
        super().__init__(conversation_state, user_state, dialog)

    async def on_members_added_activity(
            self, members_added: ChannelAccount, turn_context: TurnContext):
        for member in members_added:
            if member.id != turn_context.activity.recipient.id:
                reply = MessageFactory.text(
                    "Welcome !!"
                )
                await turn_context.send_activity(reply)

                await DialogHelper.run_dialog(
                    self.dialog,
                    turn_context,
                    self.conversation_state.create_property("DialogState"),
                )