#!/usr/bin/env python3
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import os

""" Bot Configuration """


class DefaultConfig:
    """ Bot Configuration """

    PORT = 3978
    APP_ID = os.environ.get("MicrosoftAppId", "a91e668f-42ff-4f27-89da-bd8f8d10f826")
    APP_PASSWORD = os.environ.get("MicrosoftAppPassword", "d90ac87c-fb82-47ef-b68f-53d16c118e54")
    QNA_KNOWLEDGEBASE_ID = os.environ.get("QnAKnowledgebaseId", "ead8ce0d-050b-4ac4-a8dd-8e0d5445d7c2")
    QNA_ENDPOINT_KEY = os.environ.get("QnAEndpointKey", "cba8893d-ae72-4e31-afad-13ed6e68bbf0")
    QNA_ENDPOINT_HOST = os.environ.get("QnAEndpointHostName", "https://demo-healthcare-qnamaker1.azurewebsites.net/qnamaker")
    LUIS_APP_ID = os.environ.get("LuisAppId", "6eebb9e5-4e17-4baa-941d-95117aac642b")
    LUIS_API_KEY = os.environ.get("LuisAPIKey", "825eb22e2c9c4bac80f4c45d2c1ed368")
    # LUIS endpoint host name, ie "westus.api.cognitive.microsoft.com"
    LUIS_API_HOST_NAME = os.environ.get("LuisAPIHostName", "demohealthcareluisapp1-authoring.cognitiveservices.azure.com/")
